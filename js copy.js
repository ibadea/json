const url = "http://localhost:3000/erou";
const listGroup = document.querySelector(".list-group");
// fetch(url)
//   .then((response) => response.json())
//   .then((data) => console.log(data));

// fetch(url, {
//   method: "POST",
//   body: JSON.stringify({
//     id: 0,
//     nume: "blank",
//     prenume: "blank",
//     numeErou: "blank blank",
//     superPutereId: 1,
//   }),
//   headers: {
//     "Content-type": "application/json; charset=UTF-8",
//   },
// });
// .then((response) => response.json())
// .then((data) => console.log(data));

const getPost = document.querySelector(".get-post");
const addPost = document.querySelector(".add-post");
const editPost = document.querySelector(".edit-post");
const deletePost = document.querySelector(".delete-post");

const getResponse = (response) => response.json();
const processJSON = (json) => {
  output = `
    <li class="list-group-item">id ${json.id}</li>
    <li class="list-group-item">nume ${json.nume}</li>
    <li class="list-group-item">prenume ${json.prenume}</li>
    <li class="list-group-item">nume erou ${json.numeErou}</li>
    <li class="list-group-item">id superputere ${json.superPutereId}</li>`;
  listGroup.innerHTML = output;
};
const writeServer = (action, data) => ({
  method: action,
  body: JSON.stringify(data),
  headers: {
    "Content-Type": "application/json;charset=UTF-8",
  },
});

getPost.addEventListener("click", () => {
  fetch(`${url}/10`).then(getResponse).then(processJSON);
});
addPost.addEventListener("click", () => {
  const newPost = {
    id: 10,
    nume: "x",
    prenume: "y",
    numeErou: "xy",
    superPutereId: 1,
  };
  fetch(url, writeServer("POST", newPost)).then(getResponse).then(processJSON);
});
