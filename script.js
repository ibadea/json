const listEroi = document.querySelector(".list-group");
//Get buttons
const getPost = document.querySelector(".get-post");
const addPost = document.querySelector(".add-post-form");
const nume = document.getElementById("nume");
const prenume = document.getElementById("prenume");
const numeErou = document.getElementById("numeErou");
const idSuperputere = document.getElementById("idSuperputere");
const numeSuperputere = document.getElementById("numeSuperputere");
const editPost = document.querySelector(".edit-post");
const deletePost = document.querySelector(".delete-post");

class listaEroi {
  constructor(id, nume, prenume, numeDeErou, superPuteriId, numeSuperPutere) {
    this.id = id;
    this.nume = nume;
    this.prenume = prenume;
    this.numeDeErou = numeDeErou;
    this.superPuteriId = superPuteriId;
    this.numeSuperPutere = numeSuperPutere;
  }
  arataDate() {
    return [
      this.id,
      this.nume,
      this.prenume,
      this.numeDeErou,
      this.superPuteriId,
      this.numeSuperPutere,
    ];
  }
}

const eroiArray = [];
const url = "http://localhost:3000/eroi/?_expand=superPuteri";
let output = "";

getPost.addEventListener("click", function () {
  fetch(url, { method: "GET" })
    .then(function (raspuns) {
      return raspuns.json();
    })
    .then(function (raspunsTipJson) {
      console.log("Rapuns de tip Json:", raspunsTipJson);
      output = "";
      raspunsTipJson.forEach(function (erou, index) {
        console.log(`Eroul de la indexul ${index} este ${erou.numeDeErou}`);
        eroiArray.push(
          new listaEroi(
            erou.id,
            erou.nume,
            erou.prenume,
            erou.numeDeErou,
            erou.superPuteriId,
            erou.superPuteri.numeSuperPutere
          )
        );

        output += `<li class="list-group-item"><strong>ID:</strong> ${erou.id}</li><li class="list-group-item"><strong>First Name:</strong> ${erou.nume}</li><li class="list-group-item"><strong>Last Name:</strong> ${erou.prenume}</li><li class="list-group-item"><strong>Hero Name:</strong> ${erou.numeDeErou}</li><li class="list-group-item"><strong>powers ID:</strong> ${erou.superPuteriId}</li><li class="list-group-item"><strong>Super Powers:</strong> ${erou.superPuteri.numeSuperPutere}</li><div>
          <button class="btn btn-primary edit-post" id="erou-id-${erou.id} "data-bs-toggle="modal" data-bs-target="#editHeroModal">Edit ${erou.numeDeErou}</button>
          <button class="btn btn-primary delete-post" id="erou-id-${erou.id}">Delete ${erou.numeDeErou}</button>
        </div><br>
        `;

        listEroi.innerHTML = output;
      });
      const editAllButtons = document.querySelectorAll(".edit-post");
      console.log(editAllButtons);
      editAllButtons.forEach(function (buton) {
        buton.addEventListener("click", triggerEditFlow);
      });
      const deleteAllButtons = document.querySelectorAll(".delete-post");
      console.log(deleteAllButtons);
      deleteAllButtons.forEach(function (buton) {
        buton.addEventListener("click", deleteHero);
      });
      console.log(eroiArray);
    });
});

/*identificam eroul cu id-ul "X"
implementam modalul - strict bootstrap
implementam un form in modal
formul il initializam cu eroul selectat (pasul 1)
pe submit de form punem eventlistener care: 
- construieste noul payload(obiectul erou actualizat);
- apeleaza fetch-ul care face PUT*/

//UPDATE

function triggerEditFlow(event) {
  //get hero id
  const id = Number(event.target.id.split("-").at(-1));
  //Get selected hero
  const selectedHero = eroiArray.find((hero) => hero.id === id);
  const modalBody = document.getElementById("generateForm");
  modalBody.innerHTML = generateForm(selectedHero);
  const saveButton = document.getElementById("saveEditedHero");
  saveButton.addEventListener("click", function () {
    submitHeroChanges(id);
  });
  console.log(selectedHero);
}

function submitHeroChanges(id) {
  const form = document.querySelector("#generateForm form");
  console.dir(form);
  const nume = form[0].value;
  const prenume = form[1].value;
  const numeErou = form[2].value;
  const patchURL = `http://localhost:3000/eroi/${id}`;
  console.log({ nume, prenume, numeErou, id });
  fetch(patchURL, {
    method: "PATCH",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      nume,
      prenume,
      numeDeErou: numeErou,
    }),
  });
}

function deleteHero(event) {
  const id = Number(event.target.id.split("-").at(-1));
  console.log(id);

  // console.log(event.taget.id.split("-"));

  const patchURL = `http://localhost:3000/eroi/${id}`;
  fetch(patchURL, {
    method: "DELETE",
  });
}

function generateForm(hero) {
  return `
  <form class="add-post-form" >
        <div class="mb-3">
          <label class="form-label"
            ><strong>Hero fields</strong></label
          >
        </div>
        <div class="mb-3">
          <input
            type="text"
            class="form-control"
            id="nume"
            placeholder="nume"
            value="${hero.nume}"
            required
          />
        </div>
        <div class="mb-3">
          <input
            type="text"
            class="form-control"
            id="prenume"
            placeholder="prenume"
            value="${hero.prenume}"
            required
          />
        </div>
        <div class="mb-3">
          <input
            type="text"
            class="form-control"
            id="numeErou"
            placeholder="nume de erou"
            value="${hero.numeDeErou}"
            required
          />
        </div>
      </form>
  `;
}

// EDIT;
// editPost.addEventListener("click", function (e) {
//   console.log(e.target.id);
// });

//POST
addPost.addEventListener("submit", function (e) {
  e.preventDefault();
  console.log("Form submited");
  console.log(nume.value);
  fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      nume: nume.value,
      prenume: prenume.value,
      numeDeErou: numeErou.value,
      // numeSuperPutere: numeSuperputere.value,
      superPuteriId: Number(idSuperputere.value),
    }),
  })
    .then(function (raspuns) {
      console.log(raspuns.json());
      return raspuns.json();
    })
    .catch(function (err) {
      console.log("Catch me if you ...,", err);
    });
});
